﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public enum SKILL_ADVANCEMENT_CLASS
    {
        UNDEF_SKILL_ADVANCEMENT_CLASS = 0x0,
        UNTRAINED_SKILL_ADVANCEMENT_CLASS = 0x1,
        TRAINED_SKILL_ADVANCEMENT_CLASS = 0x2,
        SPECIALIZED_SKILL_ADVANCEMENT_CLASS = 0x3,
        NUM_SKILL_ADVANCEMENT_CLASSES = 0x4,
        FORCE_SKILL_ADVANCEMENT_CLASS_32_BIT = 0x7FFFFFFF,
    };
}
