﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public enum PropertyInt64
    {
        UNDEF_INT64,
        TOTAL_EXPERIENCE_INT64,
        AVAILABLE_EXPERIENCE_INT64,
        AUGMENTATION_COST_INT64,
        ITEM_TOTAL_XP_INT64,
        ITEM_BASE_XP_INT64,
        AVAILABLE_LUMINANCE_INT64,
        MAXIMUM_LUMINANCE_INT64,
        INTERACTION_REQS_INT64,

        NUM_INT64_STAT_VALUES
    };
}
