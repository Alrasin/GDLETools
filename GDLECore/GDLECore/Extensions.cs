﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public static class Extensions
    {
        public static bool IsNullOrEmpty(this string str)
        {
            if (str == null || str.Trim().Length == 0)
                return true;
            return false;
        }

        public static List<T> GetListFromColumn<T>(this DataTable table, string columnName, bool distinct)
        {
            List<T> retVal = new List<T>();

            if (table.Columns.Contains(columnName))
            {
                foreach (DataRow dr in table.Rows)
                {
                    if (distinct == true)
                    {
                        if (!retVal.Contains(ConvertValue<T>(dr[columnName].ToString())))
                        {
                            retVal.Add(ConvertValue<T>(dr[columnName].ToString()));
                        }
                    }
                    else
                    {
                        retVal.Add(ConvertValue<T>(dr[columnName].ToString()));
                    }
                }
            }

            return retVal;
        }

        private static T ConvertValue<T>(string valueToConv)
        {
            return (T)Convert.ChangeType(valueToConv, typeof(T));
        }

        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        public static uint UnpackFromUnknown(this BinaryReader reader, uint defaultVal)
        {
            ushort val = reader.ReadUInt16();

            if ((val & 0x8000) != 0)
            {
                uint lower = reader.ReadUInt16();
                uint higher = (uint)((val & 0x3FFF) << 16);
                return (defaultVal + (higher | lower));
            }

            return (defaultVal + val);
        }
        public static string ReadGDLEString(this BinaryReader reader)
        {
            ushort stringLength = reader.ReadUInt16();
            byte[] stringValue = reader.ReadBytes(stringLength);
            int mod = (sizeof(ushort) + stringLength) % 4;
            if (mod != 0)
            {
                if (mod == 3)
                {
                    reader.ReadBytes(1);
                }
                if (mod == 2)
                {
                    reader.ReadBytes(2);
                }
                if (mod == 1)
                {
                    reader.ReadBytes(3);
                }
            }

            return Encoding.Default.GetString(stringValue);
        }


        public static void PackToUnknown(this BinaryWriter writer, uint defaultVal, uint valToPack)
        {
            uint dataID = valToPack > 0 ? valToPack : defaultVal;
            uint offset = dataID - defaultVal;

            if (offset > 0x3FFF)
            {
                if (offset > 0x3FFFFFFF)
                {
                    return;
                }
                else
                {
                    ushort low = (ushort)(((offset >> 16) & 0xffff) | 0x8000);
                    writer.Write(low);
                    ushort high = (ushort)(offset & 0xFFFF);
                    writer.Write(high);
                }
            }
            else
            {
                writer.Write(Convert.ToUInt16(offset));
            }

        }


        public static byte[] ToByteArray(this string str)
        {
            //byte[] bytes = new byte[str.Length * sizeof(char)];
            //System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            //return bytes;
            byte[] bytes = Encoding.Default.GetBytes(str);

            return bytes;
        }
    }
}
