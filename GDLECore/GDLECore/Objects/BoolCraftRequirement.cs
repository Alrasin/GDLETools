﻿using GDLECore;

using System;
using System.IO;

namespace GDLECore
{
    public class BoolCraftRequirement : IPackable<BoolCraftRequirement>
    {
        public PropertyBool Property;
        public bool Value;
        public uint Operation;  // TODO: Change to enum
        public string Message;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public BoolCraftRequirement Unpack(BinaryReader reader)
        {
            Property = (PropertyBool)reader.ReadUInt32();
            Value = Convert.ToBoolean(reader.ReadInt32());
            Operation = reader.ReadUInt32();
            Message = reader.ReadGDLEString();

            return this;
        }
    }
}
