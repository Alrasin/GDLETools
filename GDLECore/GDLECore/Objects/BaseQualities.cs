﻿using GDLECore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


namespace GDLECore
{
    public class BaseQualities : IPackable<BaseQualities>
    {
        public Dictionary<PropertyInt, uint> IntStats { get; private set; }
        public Dictionary<PropertyInt64, ulong> Int64Stats { get; private set; }
        public Dictionary<PropertyBool, bool> BoolStats { get; private set; }
        public Dictionary<PropertyFloat, double> FloatStats { get; private set; }
        public Dictionary<PropertyString, string> StringStats { get; private set; }
        public Dictionary<PropertyDID, uint> DIDStats { get; private set; }
        public Dictionary<PropertyIID, uint> IIDStats { get; private set; }
        public Dictionary<PropertyPosition, Position> PositionStats { get; private set; }

        public ItemType WeenieType = ItemType.TYPE_UNDEF;

        public BaseQualities()
        {
            IntStats = new Dictionary<PropertyInt, uint>();
            Int64Stats = new Dictionary<PropertyInt64, ulong>();
            BoolStats = new Dictionary<PropertyBool, bool>();
            FloatStats = new Dictionary<PropertyFloat, double>();
            StringStats = new Dictionary<PropertyString, string>();
            DIDStats = new Dictionary<PropertyDID, uint>();
            IIDStats = new Dictionary<PropertyIID, uint>();
            PositionStats = new Dictionary<PropertyPosition, Position>();
        }

        public BaseQualities Unpack(BinaryReader reader)
        {
            uint baseFlags = reader.ReadUInt32();
            WeenieType = (ItemType)reader.ReadUInt32();

            if ((baseFlags & 1) != 0)
            {
                ushort numProperties = reader.ReadUInt16();
                ushort sizeOfProperty = reader.ReadUInt16();

                for (int p = 0; p < numProperties; p++)
                {
                    IntStats.Add((PropertyInt)reader.ReadUInt32(), reader.ReadUInt32());
                }
            }

            if ((baseFlags & 0x80) != 0)
            {
                ushort numProperties = reader.ReadUInt16();
                ushort sizeOfProperty = reader.ReadUInt16();

                for (int p = 0; p < numProperties; p++)
                {
                    Int64Stats.Add((PropertyInt64)reader.ReadUInt32(), reader.ReadUInt64());
                }
            }

            if ((baseFlags & 2) != 0)
            {
                ushort numProperties = reader.ReadUInt16();
                ushort sizeOfProperty = reader.ReadUInt16();

                for (int p = 0; p < numProperties; p++)
                {
                    BoolStats.Add((PropertyBool)reader.ReadUInt32(), Convert.ToBoolean(reader.ReadUInt32()));
                }
            }

            if ((baseFlags & 4) != 0)
            {
                ushort numProperties = reader.ReadUInt16();
                ushort sizeOfProperty = reader.ReadUInt16();

                for (int p = 0; p < numProperties; p++)
                {
                    FloatStats.Add((PropertyFloat)reader.ReadUInt32(), reader.ReadDouble());
                }
            }
        
            if ((baseFlags & 0x10) != 0)
            {
                ushort numProperties = reader.ReadUInt16();
                ushort sizeOfProperty = reader.ReadUInt16();

                for (int p = 0; p < numProperties; p++)
                {
                    PropertyString ps = (PropertyString)reader.ReadUInt32();
                    ushort stringLength = reader.ReadUInt16();
                    byte[] stringValue = reader.ReadBytes(stringLength);
                    int mod = (sizeof(ushort) + stringLength) % 4;
                    if (mod != 0)
                    {
                        if (mod == 3)
                        {
                            reader.ReadBytes(1);
                        }
                        if (mod == 2)
                        {
                            reader.ReadBytes(2);
                        }
                        if (mod == 1)
                        {
                            reader.ReadBytes(3);
                        }

                    }
                    StringStats.Add(ps, Encoding.Default.GetString(stringValue));
                }
            }

            if ((baseFlags & 8) != 0)
            {
                ushort numProperties = reader.ReadUInt16();
                ushort sizeOfProperty = reader.ReadUInt16();

                for (int p = 0; p < numProperties; p++)
                {
                    DIDStats.Add((PropertyDID)reader.ReadUInt32(), reader.ReadUInt32());
                }
            }

            if ((baseFlags & 0x40) != 0)
            {
                ushort numProperties = reader.ReadUInt16();
                ushort sizeOfProperty = reader.ReadUInt16();

                for (int p = 0; p < numProperties; p++)
                {
                    IIDStats.Add((PropertyIID)reader.ReadUInt32(), reader.ReadUInt32());
                }
            }

            if ((baseFlags & 0x20) != 0)
            {
                ushort numPositionData = reader.ReadUInt16();
                ushort sizeOfPositionData = reader.ReadUInt16();

                for (int p = 0; p < numPositionData; p++)
                {
                    PropertyPosition ppos = (PropertyPosition)reader.ReadUInt32();
                    Position pos = new Position().Unpack(reader);
                    PositionStats.Add(ppos, pos);
                }
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            uint propFlags = 0;

            if (IntStats != null && IntStats.Count > 0)
            {
                propFlags |= 1;
            }

            if (Int64Stats != null && Int64Stats.Count > 0)
            {
                propFlags |= 0x80;
            }

            if (BoolStats != null && BoolStats.Count > 0)
            {
                propFlags |= 2;
            }

            if (FloatStats != null && FloatStats.Count > 0)
            {
                propFlags |= 4;
            }

            if (StringStats != null && StringStats.Count > 0)
            {
                propFlags |= 0x10;
            }

            if (DIDStats != null && DIDStats.Count > 0)
            {
                propFlags |= 8;
            }

            if (IIDStats != null && IIDStats.Count > 0)
            {
                propFlags |= 0x40;
            }

            if (PositionStats != null && PositionStats.Count > 0)
            {
                propFlags |= 0x20;
            }

            writer.Write(propFlags);
            writer.Write((uint)WeenieType);

            if (IntStats != null && IntStats.Count > 0)
            {
                writer.Write((ushort)IntStats.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<PropertyInt, uint> intP in IntStats)
                {
                    writer.Write((uint)intP.Key);
                    writer.Write(intP.Value);
                }
            }

            if (Int64Stats != null && Int64Stats.Count > 0)
            {
                writer.Write((ushort)Int64Stats.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<PropertyInt64, ulong> intP in Int64Stats)
                {
                    writer.Write((uint)intP.Key);
                    writer.Write(intP.Value);
                }
            }

            if (BoolStats != null && BoolStats.Count > 0)
            {
                writer.Write((ushort)BoolStats.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<PropertyBool, bool> intP in BoolStats)
                {
                    writer.Write((uint)intP.Key);
                    writer.Write(Convert.ToUInt32(intP.Value));
                }
            }

            if (FloatStats != null && FloatStats.Count > 0)
            {
                writer.Write((ushort)FloatStats.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<PropertyFloat, double> intP in FloatStats)
                {
                    writer.Write((uint)intP.Key);
                    writer.Write(intP.Value);
                }
            }

            if (StringStats != null && StringStats.Count > 0)
            {
                writer.Write((ushort)StringStats.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<PropertyString, string> intP in StringStats)
                {
                    writer.Write((uint)intP.Key);
                    ushort length = (ushort)intP.Value.Length;
                    byte[] strAsBytes = intP.Value.ToByteArray();
                    int alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);

                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                }
            }

            if (DIDStats != null && DIDStats.Count > 0)
            {
                writer.Write((ushort)DIDStats.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<PropertyDID, uint> intP in DIDStats)
                {
                    writer.Write((uint)intP.Key);
                    writer.Write(intP.Value);
                }
            }

            if (IIDStats != null && IIDStats.Count > 0)
            {
                writer.Write((ushort)IIDStats.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<PropertyIID, uint> intP in IIDStats)
                {
                    writer.Write((uint)intP.Key);
                    writer.Write(intP.Value);
                }
            }

            if (PositionStats != null && PositionStats.Count > 0)
            {
                writer.Write((ushort)PositionStats.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<PropertyPosition, Position> intP in PositionStats)
                {
                    writer.Write((uint)intP.Key);
                    intP.Value.Pack(writer);
                }
            }
        }
    }
}

