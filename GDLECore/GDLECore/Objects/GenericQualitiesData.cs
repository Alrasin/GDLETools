﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class GenericQualitiesData : IPackable<GenericQualitiesData>
    {
        public Dictionary<ulong, long> IntStatsTable { get; private set; }
        public Dictionary<ulong, int> BoolStatsTable { get; private set; }
        public Dictionary<ulong, double> FloatStatsTable { get; private set; }
        public Dictionary<ulong, string> StringStatsTable { get; private set; }

        public GenericQualitiesData()
        { }

        public GenericQualitiesData Unpack(BinaryReader reader)
        {
            uint dataFlags = reader.ReadUInt32();

            if ((dataFlags & 1) != 0)
            {
                IntStatsTable = new Dictionary<ulong, long>();
                ushort numInts = reader.ReadUInt16();
                ushort sizeInts = reader.ReadUInt16();

                for (int i = 0; i < numInts; i++)
                {
                    IntStatsTable.Add(reader.ReadUInt32(), reader.ReadUInt32());
                }
            }

            if ((dataFlags & 2) != 0)
            {
                BoolStatsTable = new Dictionary<ulong, int>();
                ushort numBools = reader.ReadUInt16();
                ushort sizeBools = reader.ReadUInt16();

                for (int b = 0; b < numBools; b++)
                {
                    BoolStatsTable.Add(reader.ReadUInt32(), reader.ReadInt32());
                }
            }

            if ((dataFlags & 4) != 0)
            {
                FloatStatsTable = new Dictionary<ulong, double>();
                ushort numFloats = reader.ReadUInt16();
                ushort sizeFloats = reader.ReadUInt16();

                for (int f = 0; f < numFloats; f++)
                {
                    FloatStatsTable.Add(reader.ReadUInt32(), reader.ReadDouble());
                }
            }

            if ((dataFlags & 8) != 0)
            {
                StringStatsTable = new Dictionary<ulong, string>();
                ushort numStrings = reader.ReadUInt16();
                ushort sizeStrings = reader.ReadUInt16();

                for (int s = 0; s < numStrings; s++)
                {
                    StringStatsTable.Add(reader.ReadUInt32(), reader.ReadString());
                }
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            uint header = 0;
            if (IntStatsTable != null)
            {
                header |= 1;
            }
            if (BoolStatsTable != null)
            {
                header |= 2;
            }
            if (FloatStatsTable != null)
            {
                header |= 4;
            }
            if (StringStatsTable != null)
            {
                header |= 8;
            }

            writer.Write(header);

            if (IntStatsTable != null)
            {
                writer.Write((ushort)IntStatsTable.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<ulong, long> intkv in IntStatsTable)
                {
                    writer.Write(intkv.Key);
                    writer.Write(intkv.Value);
                }

            }
            if (BoolStatsTable != null)
            {
                writer.Write((ushort)BoolStatsTable.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<ulong, int> intkv in BoolStatsTable)
                {
                    writer.Write(intkv.Key);
                    writer.Write(intkv.Value);
                }
            }
            if (FloatStatsTable != null)
            {
                writer.Write((ushort)FloatStatsTable.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<ulong, double> intkv in FloatStatsTable)
                {
                    writer.Write(intkv.Key);
                    writer.Write(intkv.Value);
                }
            }
            if (StringStatsTable != null)
            {
                writer.Write((ushort)StringStatsTable.Count);
                writer.Write((ushort)16);

                foreach (KeyValuePair<ulong, string> intkv in StringStatsTable)
                {
                    writer.Write(intkv.Key);
                    ushort length = (ushort)intkv.Value.Length;
                    byte[] strAsBytes = intkv.Value.ToByteArray();
                    int alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);

                    writer.Write(length);
                    writer.Write(strAsBytes);
                    for (int ab = 0; ab < alignBytes; ab++)
                    {
                        writer.Write((byte)0);
                    }
                }
            }

        }
    }
}
