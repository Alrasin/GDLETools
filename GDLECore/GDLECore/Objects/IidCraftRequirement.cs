﻿using GDLECore;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class IidCraftRequirement : IPackable<IidCraftRequirement>
    {
        public PropertyIID Property;
        public int Value;
        public uint Operation;  // TODO: Change to enum
        public string Message;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public IidCraftRequirement Unpack(BinaryReader reader)
        {
            Property = (PropertyIID)reader.ReadUInt32();
            Value = reader.ReadInt32();
            Operation = reader.ReadUInt32();
            Message = reader.ReadGDLEString();

            return this;
        }
    }
}
