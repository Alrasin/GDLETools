﻿using GDLECore;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class IntCraftMod : IPackable<IntCraftMod>
    {
        public int UnknownInt;
        public int Operation; // TODO: Make enum
        public PropertyInt Property;
        public int Value;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public IntCraftMod Unpack(BinaryReader reader)
        {
            UnknownInt = reader.ReadInt32();
            Operation = reader.ReadInt32();
            Property = (PropertyInt)reader.ReadInt32();
            Value = reader.ReadInt32();
            return this;
        }

    }
}
