﻿
using GDLECore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore
{
    public class GeneratorProfile : IPackable<GeneratorProfile>
    {
        public float Probability { get; private set; }

        [DefaultValue(WClassIDEnum.UNDEF)]
        public WClassIDEnum Type { get; private set; }

        [DefaultValue(600)]
        public double Delay { get; private set; }

        [DefaultValue(1)]
        public int InitialCreate { get; private set; }

        [DefaultValue(-1)]
        public int MaxNumber { get; private set; }

        [DefaultValue(RegenerationType.Destruction_RegenerationType)]
        public RegenerationType WhenCreate { get; private set; }

        [DefaultValue(RegenLocationType.Scatter_RegenLocationType)]
        public RegenLocationType WhereCreate { get; private set; }

        [DefaultValue(-1)]
        public int StackSize { get; private set; }

        public uint PtID { get; private set; }

        public float Shade { get; private set; }

        public Position Position { get; private set; }

        public uint Slot { get; private set;}

        public GeneratorProfile()
        { }

        public GeneratorProfile Unpack(BinaryReader reader)
        {
            Probability = reader.ReadSingle();
            Type = (WClassIDEnum)reader.ReadUInt32();
            Delay = reader.ReadDouble();
            InitialCreate = reader.ReadInt32();
            MaxNumber = reader.ReadInt32();
            WhenCreate = (RegenerationType)reader.ReadInt32();
            WhereCreate = (RegenLocationType)reader.ReadInt32();
            StackSize = reader.ReadInt32();
            PtID = reader.ReadUInt32();
            Shade = reader.ReadSingle();
            Position = new Position().Unpack(reader);
            Slot = reader.ReadUInt32();

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(Probability);
            writer.Write((uint)Type);
            writer.Write(Delay);
            writer.Write(InitialCreate);
            writer.Write(MaxNumber);
            writer.Write((uint)WhenCreate);
            writer.Write((uint)WhereCreate);
            writer.Write(StackSize);
            writer.Write(PtID);
            writer.Write(Shade);
            Position.Pack(writer);
            writer.Write(Slot);
        }
    }
}
