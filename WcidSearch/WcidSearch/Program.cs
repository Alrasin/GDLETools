﻿using GDLECore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GDLECore.Data.Database;

namespace WcidSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            // Check args
            uint wcidToFind = 0;
            if (args.Length == 0 || !UInt32.TryParse(args[0], out wcidToFind))
            {
                Console.WriteLine("No wcid found or not a number");
                return;
            }

            DBUtils.SetupConnection(ConfigurationManager.AppSettings["server"], ConfigurationManager.AppSettings["port"],
                ConfigurationManager.AppSettings["user"], ConfigurationManager.AppSettings["pass"], ConfigurationManager.AppSettings["dbname"]);

            

            // TODO create log file
            using (var wcidlog = new StreamWriter("wcid_" + wcidToFind + "_" + DateTime.Now.ToString("yyyyMMddHHmm") + ".txt"))
            {
                wcidlog.WriteLine("Scanning players for WCID: " + wcidToFind);
                wcidlog.Flush();
                Dictionary<uint, string> characterIds = DBUtils.GetCharIdsAndNames();
                foreach (KeyValuePair<uint, string> chars in characterIds)
                {
                    Weenie ChildWeenie;
                    DataTable dataTable = DBUtils.GetCharacterData(chars.Key);
                    uint weenieID = Convert.ToUInt32(dataTable.Rows[0][0]);
                    Byte[] charData = (byte[])dataTable.Rows[0][1];
                    if (charData.Length == 0)
                        continue;

                    using (BinaryReader br = new BinaryReader(new MemoryStream(charData)))
                    {
                        ChildWeenie = new Weenie(weenieID).Unpack(br);
                    }
                    DataTable inventory = DBUtils.GetCharInventory(chars.Key);
                    foreach (DataRow dr in inventory.Rows)
                    {
                        byte[] invItem = (byte[])dr[1];

                        using (BinaryReader ibr = new BinaryReader(new MemoryStream(invItem)))
                        {
                            ChildWeenie.LoadInventory(Convert.ToUInt32(dr[0].ToString().Trim()), ibr);
                        }
                    }

                    ChildWeenie.LoadPacks(inventory);

                    foreach (Weenie inv in ChildWeenie.Inventory.Values)
                    {
                        if (inv.Qualities != null && inv.Qualities.WeenieClass == (WClassIDEnum)wcidToFind)
                        {
                            wcidlog.WriteLine("WCID: " + wcidToFind + " found on " + chars.Value);
                            wcidlog.Flush();
                        }
                    }

                }

                List<uint> nonPlayerTopLvlWeenies = DBUtils.GetNonPlayerTopLevelWeenieList();

                List<Weenie> corpseWeenies = new List<Weenie>();
                List<Weenie> hookWeenies = new List<Weenie>();
                List<Weenie> storageWeenies = new List<Weenie>();

                foreach (uint npw in nonPlayerTopLvlWeenies)
                {
                    Weenie weenie;
                    DataTable dataTable = DBUtils.GetCharacterData(npw);
                    uint weenieID = Convert.ToUInt32(dataTable.Rows[0][0]);
                    Byte[] charData = (byte[])dataTable.Rows[0][1];
                    if (charData.Length == 0)
                        continue;

                    using (BinaryReader br = new BinaryReader(new MemoryStream(charData)))
                    {
                        weenie = new Weenie(weenieID).Unpack(br);
                    }

                    if (weenie.Qualities != null)
                    {
                        if (weenie.Qualities.WeenieClass == WClassIDEnum.HOOK_CEILING || weenie.Qualities.WeenieClass == WClassIDEnum.HOOK_FLOOR ||
                            weenie.Qualities.WeenieClass == WClassIDEnum.HOOK_ROOF || weenie.Qualities.WeenieClass == WClassIDEnum.HOOK_YARD ||
                            weenie.Qualities.WeenieClass == WClassIDEnum.HOOK)
                            hookWeenies.Add(weenie);
                        else if (weenie.Qualities.WeenieClass == WClassIDEnum.STORAGE)
                            storageWeenies.Add(weenie);
                        else if (weenie.Qualities.WeenieClass == WClassIDEnum.CORPSE)
                            corpseWeenies.Add(weenie);
                    }
                }

                wcidlog.WriteLine("\n");
                wcidlog.Flush();
                wcidlog.WriteLine("Scanning corpses for WCID: " + wcidToFind);
                wcidlog.Flush();
                foreach (Weenie cw in corpseWeenies)
                {
                    // Load inventory
                    DataTable inventory = DBUtils.GetCharInventory(cw.WeenieID);
                    foreach (DataRow dr in inventory.Rows)
                    {
                        byte[] invItem = (byte[])dr[1];

                        using (BinaryReader ibr = new BinaryReader(new MemoryStream(invItem)))
                        {
                            cw.LoadInventory(Convert.ToUInt32(dr[0].ToString().Trim()), ibr);
                        }
                    }

                    cw.LoadPacks(inventory);
                    foreach (Weenie inv in cw.Inventory.Values)
                    {
                        if (inv.Qualities != null && inv.Qualities.WeenieClass == (WClassIDEnum)wcidToFind)
                        {
                            wcidlog.WriteLine("WCID: " + wcidToFind + " found on Corpse of " + characterIds[cw.Qualities.IIDStats[PropertyIID.VICTIM_IID]] + 
                                " in cell " + cw.Qualities.PositionStats[PropertyPosition.INSTANTIATION_POSITION].ObjectCellId.ToString("X8"));
                            wcidlog.Flush();
                        }
                    }
                }
                wcidlog.WriteLine("\n");
                wcidlog.Flush();
                wcidlog.WriteLine("Scanning storage for WCID: " + wcidToFind);
                wcidlog.Flush();
                foreach (Weenie sw in storageWeenies)
                {
                    // Load inventory
                    DataTable inventory = DBUtils.GetCharInventory(sw.WeenieID);
                    foreach (DataRow dr in inventory.Rows)
                    {
                        byte[] invItem = (byte[])dr[1];

                        using (BinaryReader ibr = new BinaryReader(new MemoryStream(invItem)))
                        {
                            sw.LoadInventory(Convert.ToUInt32(dr[0].ToString().Trim()), ibr);
                        }
                    }

                    sw.LoadPacks(inventory);
                    foreach (Weenie inv in sw.Inventory.Values)
                    {
                        if (inv.Qualities != null && inv.Qualities.WeenieClass == (WClassIDEnum)wcidToFind)
                        {
                            wcidlog.WriteLine("WCID: " + wcidToFind + " found in chest at " + sw.Qualities.PositionStats[PropertyPosition.INSTANTIATION_POSITION].ObjectCellId.ToString("X8"));
                            wcidlog.Flush();
                        }
                    }
                }
                wcidlog.WriteLine("\n");
                wcidlog.Flush();
                wcidlog.WriteLine("Scanning hooks for WCID: " + wcidToFind);
                wcidlog.Flush();
                foreach (Weenie hw in hookWeenies)
                {
                    // Load inventory
                    DataTable inventory = DBUtils.GetCharInventory(hw.WeenieID);
                    foreach (DataRow dr in inventory.Rows)
                    {
                        byte[] invItem = (byte[])dr[1];

                        using (BinaryReader ibr = new BinaryReader(new MemoryStream(invItem)))
                        {
                            hw.LoadInventory(Convert.ToUInt32(dr[0].ToString().Trim()), ibr);
                        }
                    }

                    hw.LoadPacks(inventory);
                    foreach (Weenie inv in hw.Inventory.Values)
                    {
                        if (inv.Qualities != null && inv.Qualities.WeenieClass == (WClassIDEnum)wcidToFind)
                        {
                            wcidlog.WriteLine("WCID: " + wcidToFind + " found on hook at " + hw.Qualities.PositionStats[PropertyPosition.INSTANTIATION_POSITION].ObjectCellId.ToString("X8"));
                            wcidlog.Flush();
                        }
                    }
                }
            }
        }
    }
}
