﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdRangeBuilder
{
    public static class Globals
    {
        public static string GetIds = "SELECT id FROM weenies WHERE id > {0} LIMIT {1};";
        public static string DropIdTable = "DROP TABLE IF EXISTS `idranges`;";
        public static string CreateIdTable = "CREATE TABLE `idranges` ( `Unused` INT(11) UNSIGNED NOT NULL) COLLATE='utf8_general_ci' ENGINE=InnoDB;";
        public static string VerifyIdTableFields = "SELECT Unused FROM idranges LIMIT 1;";
    }
}
