﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdRangeBuilder
{
    public static class Database
    {
        public static void CreateConnectionString()
        {
            _connString =  new MySqlConnectionStringBuilder()
            {
                Server = System.Configuration.ConfigurationManager.AppSettings["dbserver"],
                Port = uint.Parse(System.Configuration.ConfigurationManager.AppSettings["dbport"].ToString().Trim()),
                UserID = System.Configuration.ConfigurationManager.AppSettings["dbuser"],
                Password = System.Configuration.ConfigurationManager.AppSettings["dbpass"],
                Database = System.Configuration.ConfigurationManager.AppSettings["dbname"],
                IgnorePrepare = false,
                Pooling = true,
                AllowUserVariables = true,
                AllowZeroDateTime = true
            }.ToString();
        }
        private static string _connString = string.Empty;
        private static MySqlConnection _conn = null;
        public static void ConnectAndOpen()
        {
            _conn = new MySqlConnection(_connString);
            _conn.Open();
        }

        public static void CloseDB()
        {
            if (_conn.State == ConnectionState.Open)
                _conn.Close();
        }

        public static List<UInt32> GetNextRange(UInt32 startId, int count)
        {
            List<UInt32> retVal = new List<uint>();
            DataTable data = new DataTable();

            using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetIds, startId, count), _conn))
            {
                data.Load(command.ExecuteReader());
            }
            foreach (DataRow dr in data.Rows)
            {
                retVal.Add(UInt32.Parse(dr[0].ToString().Trim()));
            }

            return retVal;
        }

        internal static void InsertRange(string insertQuery)
        {
            using (MySqlCommand command = new MySqlCommand(insertQuery, _conn))
            {
                command.ExecuteNonQuery();
            }
        }

        public static void ReCreateIDTable()
        {
            using (MySqlCommand command = new MySqlCommand(Globals.DropIdTable, _conn))
            {
                command.ExecuteNonQuery();
            }
            using (MySqlCommand command = new MySqlCommand(Globals.CreateIdTable, _conn))
            {
                command.ExecuteNonQuery();
            }
        }
    }

}
