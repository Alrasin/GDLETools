﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WeenieScrub
{
    [XmlRoot(ElementName = "key")]
    public class Key
    {
        [XmlElement(ElementName = "int")]
        public string Int { get; set; }
    }

    [XmlRoot(ElementName = "value")]
    public class Value
    {
        [XmlElement(ElementName = "boolean")]
        public string Boolean { get; set; }
        [XmlElement(ElementName = "double")]
        public string Double { get; set; }
        [XmlElement(ElementName = "int")]
        public string Int { get; set; }
        [XmlElement(ElementName = "string")]
        public string String { get; set; }
    }

    [XmlRoot(ElementName = "item")]
    public class Item
    {
        [XmlElement(ElementName = "key")]
        public Key Key { get; set; }
        [XmlElement(ElementName = "value")]
        public Value Value { get; set; }
    }

    [XmlRoot(ElementName = "BoolValues")]
    public class BoolValues
    {
        [XmlElement(ElementName = "item")]
        public Item Item { get; set; }
    }

    [XmlRoot(ElementName = "DoubleValues")]
    public class DoubleValues
    {
        [XmlElement(ElementName = "item")]
        public List<Item> Item { get; set; }
    }

    [XmlRoot(ElementName = "IntValues")]
    public class IntValues
    {
        [XmlElement(ElementName = "item")]
        public List<Item> Item { get; set; }
    }

    [XmlRoot(ElementName = "StringValues")]
    public class StringValues
    {
        [XmlElement(ElementName = "item")]
        public Item Item { get; set; }
    }

    [XmlRoot(ElementName = "ActiveSpells")]
    public class ActiveSpells
    {
        [XmlElement(ElementName = "int")]
        public List<string> Int { get; set; }
    }

    [XmlRoot(ElementName = "Spells")]
    public class Spells
    {
        [XmlElement(ElementName = "int")]
        public List<string> Int { get; set; }
    }

    [XmlRoot(ElementName = "MyWorldObject")]
    public class MyWorldObject
    {
        [XmlElement(ElementName = "HasIdData")]
        public string HasIdData { get; set; }
        [XmlElement(ElementName = "Id")]
        public string Id { get; set; }
        [XmlElement(ElementName = "LastIdTime")]
        public string LastIdTime { get; set; }
        [XmlElement(ElementName = "ObjectClass")]
        public string ObjectClass { get; set; }
        [XmlElement(ElementName = "BoolValues")]
        public BoolValues BoolValues { get; set; }
        [XmlElement(ElementName = "DoubleValues")]
        public DoubleValues DoubleValues { get; set; }
        [XmlElement(ElementName = "IntValues")]
        public IntValues IntValues { get; set; }
        [XmlElement(ElementName = "StringValues")]
        public StringValues StringValues { get; set; }
        [XmlElement(ElementName = "ActiveSpells")]
        public ActiveSpells ActiveSpells { get; set; }
        [XmlElement(ElementName = "Spells")]
        public Spells Spells { get; set; }
    }
}
