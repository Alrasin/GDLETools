﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeenieScrub
{
    public class vgiweenie
    {
        public Dictionary<uint, uint> intProperties = new Dictionary<uint, uint>();
        public Dictionary<uint, string> stringProperties = new Dictionary<uint, string>();
        public Dictionary<uint, bool> boolProperties = new Dictionary<uint, bool>();
        public Dictionary<uint, double> doubleProperties = new Dictionary<uint, double>();
        public Dictionary<uint, UInt64> int64Properties = new Dictionary<uint, ulong>();
        public List<uint> Spells = new List<uint>();
        public uint unk;
        public uint ObjectClass;
        
    }
}
