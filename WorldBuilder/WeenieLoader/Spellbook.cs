using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weenies.Types
{
    public partial class Spellbook
    {
        [JsonProperty("key")]
        public long Key { get; set; }

        [JsonProperty("value")]
        public SpellbookValue Value { get; set; }
    }

    public partial class SpellbookValue
    {
        [JsonProperty("casting_likelihood")]
        public double CastingLikelihood { get; set; }
    }
}
