using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Weenies.Types
{
    public class Stat
    {
        [JsonProperty("key")]
        public long Key { get; set; }

        [JsonProperty("value")]
        public long Value { get; set; }
    }

    public class StringStat
    {
        [JsonProperty("key")]
        public long Key { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

}
