
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Weenies.Types;

namespace Weenies
{
    public class Weenie
    {
        [JsonProperty("attributes")]
        public Attributes Attributes { get; set; }

        [JsonProperty("body")]
        public Body Body { get; set; }

        [JsonProperty("boolStats")]
        public List<Stat> BoolStats { get; set; }

        [JsonProperty("createList")]
        public List<CreateList> CreateList { get; set; }

        [JsonProperty("didStats")]
        public List<Stat> DidStats { get; set; }

        [JsonProperty("emoteTable")]
        public List<EmoteTable> EmoteTable { get; set; }

        [JsonProperty("eventFilter")]
        public EventFilter EventFilter { get; set; }

        [JsonProperty("floatStats")]
        public List<Stat> FloatStats { get; set; }

        [JsonProperty("generatorTable")]
        public List<GeneratorTable> GeneratorTable { get; set; }

        [JsonProperty("intStats")]
        public List<Stat> IntStats { get; set; }

        [JsonProperty("pageDataList")]
        public PageDataList PageDataList { get; set; }

        [JsonProperty("posStats")]
        public List<PosStat> PosStats { get; set; }

        [JsonProperty("skills")]
        public List<Skill> Skills { get; set; }

        [JsonProperty("spellbook")]
        public List<Spellbook> Spellbook { get; set; }

        [JsonProperty("stringStats")]
        public List<StringStat> StringStats { get; set; }

        [JsonProperty("wcid")]
        public long Wcid { get; set; }

        [JsonProperty("weenieType")]
        public long WeenieType { get; set; }

        public static Weenie FromJson(string json) => JsonConvert.DeserializeObject<Weenie>(json, Converter.Settings);


    }

    public static class Serialize
    {
        public static string ToJson(this Weenie self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    public class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
