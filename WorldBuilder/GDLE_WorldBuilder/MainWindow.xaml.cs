﻿using CachePwnLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WeenieLoader;

namespace GDLE_WorldBuilder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static CraftingData craftingData = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MenuSettings_Click(object sender, RoutedEventArgs e)
        {
            Settings settings = new Settings();
            settings.Show();
        }

        private void RecipeToolbar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Common.PathToCraftBin))
            {
                MessageBox.Show("Please set path to Crafting binary file in Settings");
                return;
            }
            GenericProgressBar genericProgressBar = new GenericProgressBar(System.IO.Path.GetFileName(Common.PathToCraftBin));
            genericProgressBar.Show();
            WeenieData wd = new WeenieData(JsonSettings.WeenieJsonPath);
            craftingData = new CraftingData();

            genericProgressBar.Close();

        }

        private void searchbutton_Click(object sender, RoutedEventArgs e)
        {
            Search search = new Search("");
            search.Show();
            //LoadRecipe(search.SelectedRecipe);
        }

        private void RecipeToolButton_Click(object sender, RoutedEventArgs e)
        {
            Search search = new Search("Tool");
            search.Show();
        }

        private void JsonLocationSettings_Click(object sender, RoutedEventArgs e)
        {
            JsonSettings jsonSettings = new JsonSettings();
            jsonSettings.Show();
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {

        }

        private void RecipeExpander_Expanded(object sender, RoutedEventArgs e)
        {

        }

        private void RequirementsExpander_Expanded(object sender, RoutedEventArgs e)
        {

        }

        private void RequirementsExpander_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        private void RecipeToolButton3_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
