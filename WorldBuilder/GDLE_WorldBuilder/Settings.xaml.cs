﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GDLE_WorldBuilder
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : Window
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void CraftBinLocation_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                if (File.Exists(openFileDialog.FileName))
                {
                    CachePwnLoader.Common.PathToCraftBin = openFileDialog.FileName;
                    CraftBinPath.Content = openFileDialog.FileName;
                }
                else
                    MessageBox.Show("Unable to open Crafting bin file");
            }

        }

        private void CloseSettings_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
