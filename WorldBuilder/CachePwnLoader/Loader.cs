﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CachePwnLoader
{
    public class Loader
    {
        public Loader(BinType binType, string pathToBinFile)
        {
            if (File.Exists(pathToBinFile))
            {
                switch (binType)
                {
                    case BinType.Crafting:
                        Common.PathToCraftBin = pathToBinFile;
                        break;
                    default:
                        throw new ArgumentException("Bin file not implemented yet");
                }
            }
        }
    }
}
