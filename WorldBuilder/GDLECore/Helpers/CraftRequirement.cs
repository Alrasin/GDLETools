﻿using GDLECore.Enums;
using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Helpers
{
    public class CraftRequirement<T, U> : IPackable<CraftRequirement<T, U>> where T: Enum 
    {   
        public T Property;
        public U Value;
        public uint Operation;  // TODO: Change to enum
        public string Message;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public CraftRequirement<T, U> Unpack(BinaryReader reader)
        {
            switch (Property.GetType())
            {
                //case PropertyInt:
                //    Property = (PropertyInt)reader.ReadUInt32();
                //    Value = reader.ReadUInt32();
                //    Operation = reader.ReadUInt32();
                //    Message = reader.ReadString();
                //    break;
                default:
                    break;
            }

            return this;
        }
    }
}
