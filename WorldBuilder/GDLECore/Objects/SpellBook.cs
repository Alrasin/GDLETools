﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class SpellBook : IPackable<SpellBook>
    {
        public Dictionary<uint, SpellBookPage> SpellList { get; private set; }

        public SpellBook()
        {
            SpellList = new Dictionary<uint, SpellBookPage>();
        }

        public SpellBook Unpack(BinaryReader reader)
        {
            ushort numSpells = reader.ReadUInt16();
            ushort sizeSpell = reader.ReadUInt16();

            for (int s = 0; s < numSpells; s++)
            {
                SpellList.Add(reader.ReadUInt32(), new SpellBookPage().Unpack(reader));
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((ushort)SpellList.Count);
            writer.Write((ushort)16);

            foreach (KeyValuePair<uint, SpellBookPage> sp in SpellList)
            {
                writer.Write(sp.Key);
                sp.Value.Pack(writer);
            }
        }
    }
}
