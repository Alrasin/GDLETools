﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class Quest : IPackable<Quest>
    {
        public string Name { get; private set; }
        public double LastUpdate { get; private set; }
        public uint RealTime { get; private set; }
        public uint NumberOfCompletions { get; private set; }

        public Quest()
        { }

        public Quest Unpack(BinaryReader reader)
        {
            ushort stringLength = reader.ReadUInt16();
            byte[] stringValue = reader.ReadBytes(stringLength);
            int mod = (sizeof(ushort) + stringLength) % 4;
            if (mod != 0)
            {
                if (mod == 3)
                {
                    reader.ReadBytes(1);
                }
                if (mod == 2)
                {
                    reader.ReadBytes(2);
                }
                if (mod == 1)
                {
                    reader.ReadBytes(3);
                }

            }
            Name =  Encoding.Default.GetString(stringValue);
            LastUpdate = reader.ReadDouble();
            RealTime = reader.ReadUInt32();
            NumberOfCompletions = reader.ReadUInt32();

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            ushort length = (ushort)Name.Length;
            byte[] strAsBytes = Name.ToByteArray();
            int alignBytes = CommonCore.ByteAlign(sizeof(ushort) + strAsBytes.Length);

            writer.Write(length);
            writer.Write(strAsBytes);
            for (int ab = 0; ab < alignBytes; ab++)
            {
                writer.Write((byte)0);
            }

            writer.Write(LastUpdate);
            writer.Write(RealTime);
            writer.Write(NumberOfCompletions);
        }
    }
}
