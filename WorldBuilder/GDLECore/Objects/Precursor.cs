﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class Precursor
    {
        public uint Tool;
        public uint Target;
        public uint RecipeID;
    }
}
