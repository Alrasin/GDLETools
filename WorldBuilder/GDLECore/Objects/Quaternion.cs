﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class Quaternion : IPackable<Quaternion>
    {
        public float W { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }

        public Quaternion()
        { }

        public Quaternion Unpack(BinaryReader reader)
        {
            W = reader.ReadSingle();
            X = reader.ReadSingle();
            Y = reader.ReadSingle();
            Z = reader.ReadSingle();
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(W);
            writer.Write(X);
            writer.Write(Y);
            writer.Write(Z);
        }
    }
}
