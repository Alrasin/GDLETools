﻿using System.IO;
using GDLECore.Interfaces;

namespace GDLECore.Objects
{
    public class CreationProfile : IPackable<CreationProfile>
    {
        public uint WCID { get; private set; }

        public uint TryToBond { get; private set; }
        public uint Palette { get; private set; }

        public float ShadeProbability { get; private set; }

        public uint Destination { get; private set; }

        public uint StackSize { get; private set; }

        public CreationProfile()
        { }

        public CreationProfile Unpack(BinaryReader reader)
        {
            WCID = reader.ReadUInt32();
            Palette = reader.ReadUInt32();
            ShadeProbability = reader.ReadSingle();
            Destination = reader.ReadUInt32();
            StackSize = reader.ReadUInt32();
            TryToBond = reader.ReadUInt32();
            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(WCID);
            writer.Write(Palette);
            writer.Write(ShadeProbability);
            writer.Write(Destination);
            writer.Write(StackSize);
            writer.Write(TryToBond);
        }
    }
}
