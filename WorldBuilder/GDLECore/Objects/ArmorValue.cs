﻿using System.IO;
using GDLECore.Interfaces;

namespace GDLECore.Objects
{
    public class ArmorValue : IPackable<ArmorValue>
    {
        public uint BaseArmor { get; private set; }
        public uint VsSlash { get; private set; }
        public uint VsPierce { get; private set; }
        public uint VsBlud { get; private set; }
        public uint VsCold { get; private set; }
        public uint VsFire { get; private set; }
        public uint VsAcid { get; private set; }
        public uint VsElectric { get; private set; }
        public uint VsNether { get; private set; }

        public ArmorValue()
        { }

        public ArmorValue Unpack(BinaryReader reader)
        {
            BaseArmor = reader.ReadUInt32();
            VsSlash = reader.ReadUInt32();
            VsPierce = reader.ReadUInt32();
            VsBlud = reader.ReadUInt32();
            VsCold = reader.ReadUInt32();
            VsFire = reader.ReadUInt32();
            VsAcid = reader.ReadUInt32();
            VsElectric = reader.ReadUInt32();
            VsNether = reader.ReadUInt32();

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(BaseArmor);
            writer.Write(VsSlash);
            writer.Write(VsPierce);
            writer.Write(VsBlud);
            writer.Write(VsCold);
            writer.Write(VsFire);
            writer.Write(VsAcid);
            writer.Write(VsElectric);
            writer.Write(VsNether);
        }
    }
}
