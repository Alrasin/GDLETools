﻿using System.Collections.Generic;
using System.IO;
using GDLECore.Interfaces;

namespace GDLECore.Objects
{
    public class EmoteSet : IPackable<EmoteSet>
    {
        public EmoteCategory Category { get; private set; }
        public float Probability { get; private set; }
        public uint ClassID { get; private set; }
        public string QuestName { get; private set; }
        public uint Style { get; private set; }
        public uint SubStyle { get; private set; }
        public uint VendorType { get; private set; }
        public float MinHealth { get; private set; }
        public float MaxHealth { get; private set; }

        public List<Emote> Emotes { get; private set; }

        public EmoteSet()
        {
            Emotes = new List<Emote>();
        }

        public EmoteSet Unpack(BinaryReader reader)
        {
            Category = (EmoteCategory)reader.ReadUInt32();
            Probability = reader.ReadSingle();

            switch (Category)
            {
                case EmoteCategory.Refuse_EmoteCategory:
                case EmoteCategory.Give_EmoteCategory:
                    ClassID = reader.ReadUInt32();
                    break;

                case EmoteCategory.HeartBeat_EmoteCategory:
                    Style = reader.ReadUInt32();
                    SubStyle = reader.ReadUInt32();
                    break;

                case EmoteCategory.QuestSuccess_EmoteCategory: // 12 0xC
                case EmoteCategory.QuestFailure_EmoteCategory: // 13 0xD
                case EmoteCategory.TestSuccess_EmoteCategory: // 22 0x16
                case EmoteCategory.TestFailure_EmoteCategory: // 23 0x17
                case EmoteCategory.EventSuccess_EmoteCategory: // 27 0x1B
                case EmoteCategory.EventFailure_EmoteCategory: // 28 0x1C
                case EmoteCategory.TestNoQuality_EmoteCategory: // 29 0x1D
                case EmoteCategory.QuestNoFellow_EmoteCategory: // 30 0x1E
                case EmoteCategory.TestNoFellow_EmoteCategory: // 31 0x1F
                case EmoteCategory.GotoSet_EmoteCategory: // 32 0x20
                case EmoteCategory.NumFellowsSuccess_EmoteCategory: // 33 0x21
                case EmoteCategory.NumFellowsFailure_EmoteCategory: // 34 0x22
                case EmoteCategory.NumCharacterTitlesSuccess_EmoteCategory: // 35 0x23
                case EmoteCategory.NumCharacterTitlesFailure_EmoteCategory: // 36 0x24
                case EmoteCategory.ReceiveLocalSignal_EmoteCategory: // 37 0x25
                case EmoteCategory.ReceiveTalkDirect_EmoteCategory: // 38 0x26
                    QuestName = reader.ReadString();
                    break;

                case EmoteCategory.Vendor_EmoteCategory:
                    VendorType = reader.ReadUInt32();
                    break;

                case EmoteCategory.WoundedTaunt_EmoteCategory:
                    MinHealth = reader.ReadSingle();
                    MaxHealth = reader.ReadSingle();
                    break;
            }

            uint numOfEmote = reader.ReadUInt32();

            for (int e = 0; e < numOfEmote; e++)
            {
                Emotes.Add(new Emote().Unpack(reader));
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((uint)Category);
            writer.Write(Probability);

            switch (Category)
            {
                case EmoteCategory.Refuse_EmoteCategory:
                case EmoteCategory.Give_EmoteCategory:
                    writer.Write(ClassID);
                    break;

                case EmoteCategory.HeartBeat_EmoteCategory:
                    writer.Write(Style);
                    writer.Write(SubStyle);
                    break;

                case EmoteCategory.QuestSuccess_EmoteCategory: // 12 0xC
                case EmoteCategory.QuestFailure_EmoteCategory: // 13 0xD
                case EmoteCategory.TestSuccess_EmoteCategory: // 22 0x16
                case EmoteCategory.TestFailure_EmoteCategory: // 23 0x17
                case EmoteCategory.EventSuccess_EmoteCategory: // 27 0x1B
                case EmoteCategory.EventFailure_EmoteCategory: // 28 0x1C
                case EmoteCategory.TestNoQuality_EmoteCategory: // 29 0x1D
                case EmoteCategory.QuestNoFellow_EmoteCategory: // 30 0x1E
                case EmoteCategory.TestNoFellow_EmoteCategory: // 31 0x1F
                case EmoteCategory.GotoSet_EmoteCategory: // 32 0x20
                case EmoteCategory.NumFellowsSuccess_EmoteCategory: // 33 0x21
                case EmoteCategory.NumFellowsFailure_EmoteCategory: // 34 0x22
                case EmoteCategory.NumCharacterTitlesSuccess_EmoteCategory: // 35 0x23
                case EmoteCategory.NumCharacterTitlesFailure_EmoteCategory: // 36 0x24
                case EmoteCategory.ReceiveLocalSignal_EmoteCategory: // 37 0x25
                case EmoteCategory.ReceiveTalkDirect_EmoteCategory: // 38 0x26
                    writer.Write(QuestName);
                    break;

                case EmoteCategory.Vendor_EmoteCategory:
                    writer.Write(VendorType);
                    break;

                case EmoteCategory.WoundedTaunt_EmoteCategory:
                    writer.Write(MinHealth);
                    writer.Write(MaxHealth);
                    break;
            }

            writer.Write(Emotes.Count);
            foreach (Emote e in Emotes)
                e.Pack(writer);

        }

    }
}
