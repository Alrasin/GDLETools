﻿using GDLECore.Enums;
using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class DidCraftMod : IPackable<DidCraftMod>
    {
        public int UnknownInt;
        public int Operation; // TODO: Make enum
        public PropertyDID Property;
        public int Value;

        public void Pack(BinaryWriter writer)
        {
            throw new NotImplementedException();
        }

        public DidCraftMod Unpack(BinaryReader reader)
        {
            UnknownInt = reader.ReadInt32();
            Operation = reader.ReadInt32();
            Property = (PropertyDID)reader.ReadInt32();
            Value = reader.ReadInt32();
            return this;
        }

    }
}
