﻿using System.IO;
using System.Collections.Generic;
using GDLECore.Interfaces;

namespace GDLECore.Objects
{
    public class Body : IPackable<Body>
    {
        public Dictionary<uint, BodyPart> BodyPartTable { get; private set; }

        public Body()
        {
            BodyPartTable = new Dictionary<uint, BodyPart>();
        }

        public Body Unpack(BinaryReader reader)
        {
            ushort numBodyParts = reader.ReadUInt16();
            ushort sizeOfBodyParts = reader.ReadUInt16();

            for (uint b = 0; b < numBodyParts; b++)
            {
                BodyPart bp = new BodyPart().Unpack(reader);
                BodyPartTable.Add(b, bp);
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((ushort)BodyPartTable.Count);
            writer.Write((ushort)16);

            foreach (KeyValuePair<uint, BodyPart> bd in BodyPartTable)
            {
                writer.Write(bd.Key);
                bd.Value.Pack(writer);
            }
        }
    }
}
