﻿using GDLECore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDLECore.Objects
{
    public class SecondaryAttribute : Attribute, IPackable<SecondaryAttribute>
    {
        public uint CurrentValue { get; private set; }

        public new SecondaryAttribute Unpack(BinaryReader reader)
        {
            base.Unpack(reader);

            CurrentValue = reader.ReadUInt32();

            return this;
        }

        public new void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(CurrentValue);
        }

    }
}
