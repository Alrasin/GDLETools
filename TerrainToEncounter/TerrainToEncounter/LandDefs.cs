﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TerrainToEncounter
{
    public static class LandDefs
    {
        public static int gid_to_lcoord(uint cell_id, ref int x, ref int y)
        {
            int result; // eax

            result = 0;
            if (inbound_valid_cellid(cell_id))
            {
                if ((ushort)cell_id < 0x100u)
                {
                    x = (int)((cell_id >> 21) & 0x7F8);
                    y = (byte)(cell_id >> 16) * 8; // y = 8 * BYTE2(cell_id);
                    x += ((ushort)cell_id - 1) >> 3;
                    y = (((byte)cell_id - 1) & 7) + y;
                    if (x >= 0 && y >= 0 && x < 2040 && y < 2040)
                    {
                        result = 1;
                    }
                }
            }
            return result;
        }

        public static bool inbound_valid_cellid(uint cell_id)
        {
            int xBoundary; // ecx
            int yBoundary; // eax
            bool result; // eax

            result = false;
            if ((ushort)cell_id >= 1u && (ushort)cell_id <= 0x40u || (ushort)cell_id >= 0x100u && (ushort)cell_id <= 0xFFFDu || (ushort)cell_id == 0xFFFF)
            {
                xBoundary = (int)((cell_id >> 21) & 0x7F8);
                yBoundary = (int)((cell_id >> 13) & 0x7F8);
                if (xBoundary >= 0 && yBoundary >= 0 && xBoundary < 2040 && yBoundary < 2040)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
