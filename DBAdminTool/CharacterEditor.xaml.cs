﻿using DBAdminTool.Enums;
using DBAdminTool.Objects;
using DBAdminTool.WindowObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DBAdminTool
{
    /// <summary>
    /// Interaction logic for CharacterEditor.xaml
    /// </summary>
    public partial class CharacterEditor : Window
    {

        public Weenie weenie { get; set; }
        private Weenie selectedWeenie { get; set; }

        private List<StatDetail> IntStatDetail = new List<StatDetail>();
        private List<StatDetail> Int64StatDetail = new List<StatDetail>();
        private List<StatDetail> BoolStatDetail = new List<StatDetail>();
        private List<StatDetail> FloatStatDetail = new List<StatDetail>();
        private List<StatDetail> StringStatDetail = new List<StatDetail>();
        private List<StatDetail> DIDStatDetail = new List<StatDetail>();
        private List<StatDetail> IIDStatDetail = new List<StatDetail>();
        private List<StatDetail> PositionStatDetail = new List<StatDetail>();

        private CharacterSelect charSelect;
        private List<ItemDetail> packItems = new List<ItemDetail>();
        private int PackInstance = 0;

        private List<string> specedSkillList = new List<string>();
        private List<string> trainedSkillList = new List<string>();

        public CharacterEditor()
        {
            statdatagrid = new DataGrid();
            InitializeComponent();
            ShowWeenie();
        }

        public CharacterEditor(Weenie testweenie) 
        {
            weenie = testweenie;
            InitializeComponent();
            ShowWeenie();
        }


        private void CloseConnections()
        {
            MessageBox.Show("Close any file or db connections open");
        }



        private void ShowError(string errorMsg)
        {
            MessageBox.Show(errorMsg, "Application Error", MessageBoxButton.OK);
        }


        public void ShowWeenie()
        {
            // Find character name & level
            charName.Content = "Name: " + weenie.Qualities.StringStats[PropertyString.NAME_STRING];
            charLevel.Content = "Level: " + weenie.Qualities.IntStats[PropertyInt.LEVEL_INT];

            // Attributes
            CharStrength.Content = weenie.Qualities.AttributeCache.Strength.InitialLevel;
            CharEndurance.Content = weenie.Qualities.AttributeCache.Endurance.InitialLevel;
            CharCoord.Content = weenie.Qualities.AttributeCache.Coordination.InitialLevel;
            CharQuickness.Content = weenie.Qualities.AttributeCache.Quickness.InitialLevel;
            CharFocus.Content = weenie.Qualities.AttributeCache.Focus.InitialLevel;
            CharSelf.Content = weenie.Qualities.AttributeCache.Self.InitialLevel;
            CharHealth.Content = weenie.Qualities.AttributeCache.Health.CurrentValue;
            CharStamina.Content = weenie.Qualities.AttributeCache.Stamina.CurrentValue;
            CharMana.Content = weenie.Qualities.AttributeCache.Mana.CurrentValue;

            // Packs
            int packCount = weenie.Packs.Count;
            if (packCount > 0)
                PackOne.IsEnabled = true;
            if (packCount > 1)
                PackTwo.IsEnabled = true;
            if (packCount > 2)
                PackThree.IsEnabled = true;
            if (packCount > 3)
                PackFour.IsEnabled = true;
            if (packCount > 4)
                PackFive.IsEnabled = true;
            if (packCount > 5)
                PackSix.IsEnabled = true;
            if (packCount > 6)
                PackSeven.IsEnabled = true;

            foreach (KeyValuePair<SkillList, Skill> skill in weenie.Qualities.Skills)
            {
                if (skill.Value.SkillLevel == SKILL_ADVANCEMENT_CLASS.SPECIALIZED_SKILL_ADVANCEMENT_CLASS)
                {
                    specedSkillList.Add(skill.Key.ToString());
                }
                if (skill.Value.SkillLevel == SKILL_ADVANCEMENT_CLASS.TRAINED_SKILL_ADVANCEMENT_CLASS)
                {
                    trainedSkillList.Add(skill.Key.ToString());
                }
            }

            specSkills.ItemsSource = specedSkillList;
            trainedSkills.ItemsSource = trainedSkillList;

        }

        private void MainwindowLoadChar_Click(object sender, RoutedEventArgs e)
        {
            if (!Application.Current.Properties.Contains("dbconnectionvalid"))
            {
                ShowError("No valid database connected");
                return;
            }

            charSelect = new CharacterSelect(DBUtils.GetCharacters(Application.Current.Properties["dbconnectionstring"].ToString()))
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };

            charSelect.Show();
            charSelect.CharacterSelected += new EventHandler(CharacterLoad);

        }

        private void CharacterLoad(object sender, EventArgs e)
        {
            ShowWeenie();
        }

        private void MainPack_Click(object sender, RoutedEventArgs e)
        {
            ClearDataGrids();
            packItems = new List<ItemDetail>();
            foreach (KeyValuePair<uint, Weenie> w in weenie.Inventory)
            {
                packItems.Add(new ItemDetail()
                {
                    Name = w.Value.Qualities.StringStats[PropertyString.NAME_STRING],
                    GUID = w.Value.WeenieID.ToString()
                });
            }
            packSelectionItems.ItemsSource = packItems;
            PackInstance = 0;
        }


        private void packContents_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (ListView)sender;
            if (item != null)
            {
                if (item.SelectedValue != null)
                {
                    string itemSelected = item.SelectedValue.ToString();

                    if (PackInstance > 0)
                    {
                        selectedWeenie = (from d in weenie.Packs.ElementAt(PackInstance - 1).Value.Inventory
                                          where d.Value.Qualities.StringStats[PropertyString.NAME_STRING] == itemSelected
                                          select d).FirstOrDefault().Value;
                    }
                    else if (PackInstance == 0)
                    {
                        selectedWeenie = (from d in weenie.Inventory
                                          where d.Value.Qualities.StringStats[PropertyString.NAME_STRING] == itemSelected
                                          select d).FirstOrDefault().Value;
                    }
                    else
                    {
                        selectedWeenie = (from d in weenie.Equipment
                                          where d.Value.Qualities.StringStats[PropertyString.NAME_STRING] == itemSelected
                                          select d).FirstOrDefault().Value;
                    }

                    if (selectedWeenie != null)
                    {
                        ClearDataGrids(true, false);
                        ResetLists();

                        packSelectedName.Content = selectedWeenie.Qualities.StringStats[PropertyString.NAME_STRING];
                        packSelectedWID.Content = selectedWeenie.WeenieID.ToString();

                        foreach (KeyValuePair<PropertyInt, uint> intst in selectedWeenie.Qualities.IntStats)
                            IntStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyInt64, ulong> intst in selectedWeenie.Qualities.Int64Stats)
                            Int64StatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyBool, bool> intst in selectedWeenie.Qualities.BoolStats)
                            BoolStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyFloat, double> intst in selectedWeenie.Qualities.FloatStats)
                            FloatStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyString, string> intst in selectedWeenie.Qualities.StringStats)
                            StringStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value });
                        foreach (KeyValuePair<PropertyDID, uint> intst in selectedWeenie.Qualities.DIDStats)
                            DIDStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyIID, uint> intst in selectedWeenie.Qualities.IIDStats)
                            IIDStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyPosition, Position> intst in selectedWeenie.Qualities.PositionStats)
                            PositionStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });

                        intStatTab.IsSelected = true;
                        statdatagrid.ItemsSource = IntStatDetail;

                    }
                    else
                        MessageBox.Show($"Unable to load {itemSelected}. Please add error log to issues in GitLab for GDLE");
                }
            }
        }

        private void PackOne_Click(object sender, RoutedEventArgs e)
        {
            GetPackContents(0);
        }

        private void GetPackContents(uint packIndex)
        {
            ClearDataGrids();
            packItems = new List<ItemDetail>();

            if (weenie.Packs.Count < (int)packIndex)
                return;
            Weenie packId;
            try
            {
                packId = weenie.Packs[packIndex];
            }
            catch
            {
                return;
            }

            foreach (KeyValuePair<uint, Weenie> w in packId.Inventory)
            {
                packItems.Add(new ItemDetail()
                {
                    Name = w.Value.Qualities.StringStats[PropertyString.NAME_STRING],
                    GUID = w.Value.WeenieID.ToString()
                });
            }

            packSelectionItems.ItemsSource = packItems;
            PackInstance = 1;
        }

        private void PackTwo_Click(object sender, RoutedEventArgs e)
        {
            GetPackContents(1);
        }


        private void PackThree_Click(object sender, RoutedEventArgs e)
        {
            GetPackContents(2);
        }

        private void PackFour_Click(object sender, RoutedEventArgs e)
        {
            GetPackContents(3);

        }

        private void PackFive_Click(object sender, RoutedEventArgs e)
        {
            GetPackContents(4);
        }

        private void PackSix_Click(object sender, RoutedEventArgs e)
        {
            GetPackContents(5);
        }


        private void PackEight_Click(object sender, RoutedEventArgs e)
        {
            GetPackContents(7);
        }

        private void PackSeven_Click(object sender, RoutedEventArgs e)
        {
            GetPackContents(6);
        }

        private void ClearDataGrids(bool statGrid = true, bool itemGrid = true)
        {
            if (statGrid)
            {
                statdatagrid.ItemsSource = null;
                statdatagrid.Items.Clear();
            }
            if (itemGrid)
            {
                packSelectionItems.ItemsSource = null;
                packSelectionItems.Items.Clear();
            }
        }

        private void ResetLists()
        {
            IntStatDetail = new List<StatDetail>();
            Int64StatDetail = new List<StatDetail>();
            BoolStatDetail = new List<StatDetail>();
            FloatStatDetail = new List<StatDetail>();
            StringStatDetail = new List<StatDetail>();
            DIDStatDetail = new List<StatDetail>();
            IIDStatDetail = new List<StatDetail>();
            PositionStatDetail = new List<StatDetail>();
        }

        private void WornPack_Click(object sender, RoutedEventArgs e)
        {
            ClearDataGrids();
            packItems = new List<ItemDetail>();

            foreach (KeyValuePair<uint, Weenie> w in weenie.Equipment)
            {
                packItems.Add(new ItemDetail()
                {
                    Name = w.Value.Qualities.StringStats[PropertyString.NAME_STRING],
                    GUID = w.Value.WeenieID.ToString()
                });
            }

            packSelectionItems.ItemsSource = packItems;
            PackInstance = -1;
        }

        private void delStat_Click(object sender, RoutedEventArgs e)
        {
            if (statdatagrid.SelectedIndex > -1)
            {
                // Get Selected Type

                // Get Stat Type
                if (intStatTab.IsSelected)
                {
                    var cells = (StatDetail)statdatagrid.SelectedItem;
                    if (intStatTab.IsSelected)
                    {
                        DeleteIntStat(cells);
                    }
                    else if (int64StatTab.IsSelected)
                    { }
                    else if (boolStatTab.IsSelected)
                    { }
                    else if (floatStatTab.IsSelected)
                    { }
                    else if (stringStatTab.IsSelected)
                    { }
                    else if (didStatTab.IsSelected)
                    { }
                    else if (iidStatTab.IsSelected)
                    { }
                    else if (posStatTab.IsSelected)
                    { }
                    else
                    {
                        MessageBox.Show("Deleting stats from this tab is not supported yet");
                    }

                }
                //  property type
                // Remove from list
                // Refresh datagrid
            }
        }

        private void DeleteIntStat(StatDetail item)
        {
            PropertyInt propertyInt = (PropertyInt)Enum.Parse(typeof(PropertyInt), item.EnumName);
            if (selectedWeenie.Qualities.IntStats.ContainsKey(propertyInt))
            {
                selectedWeenie.Qualities.IntStats.Remove(propertyInt);
            }
            ClearDataGrids();
            statdatagrid.ItemsSource = IntStatDetail;
        }

        private void editStat_Click(object sender, RoutedEventArgs e)
        {

        }

        private void addStat_Click(object sender, RoutedEventArgs e)
        {

        }

        private void charDataShow_Click(object sender, RoutedEventArgs e)
        {
            ClearDataGrids();
            ResetLists();
            packItems = new List<ItemDetail>();
            packSelectionItems.ItemsSource = packItems;
            selectedWeenie = weenie;
            foreach (KeyValuePair<PropertyInt, uint> intst in weenie.Qualities.IntStats)
                IntStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
            foreach (KeyValuePair<PropertyInt64, ulong> intst in weenie.Qualities.Int64Stats)
                Int64StatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
            foreach (KeyValuePair<PropertyBool, bool> intst in weenie.Qualities.BoolStats)
                BoolStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
            foreach (KeyValuePair<PropertyFloat, double> intst in weenie.Qualities.FloatStats)
                FloatStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
            foreach (KeyValuePair<PropertyString, string> intst in weenie.Qualities.StringStats)
                StringStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value });
            foreach (KeyValuePair<PropertyDID, uint> intst in weenie.Qualities.DIDStats)
                DIDStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
            foreach (KeyValuePair<PropertyIID, uint> intst in weenie.Qualities.IIDStats)
                IIDStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
            foreach (KeyValuePair<PropertyPosition, Position> intst in weenie.Qualities.PositionStats)
                PositionStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });

            intStatTab.IsSelected = true;
            statdatagrid.ItemsSource = IntStatDetail;
        }

        private void statsTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ClearDataGrids();
            if (intStatTab.IsSelected)
            {
                statdatagrid.ItemsSource = IntStatDetail;
            }
            if (int64StatTab.IsSelected)
            {
                statdatagrid.ItemsSource = Int64StatDetail;
            }
            if (boolStatTab.IsSelected)
            {
                statdatagrid.ItemsSource = BoolStatDetail;
            }
            if (floatStatTab.IsSelected)
            {
                statdatagrid.ItemsSource = FloatStatDetail;
            }
            if (stringStatTab.IsSelected)
            {
                statdatagrid.ItemsSource = StringStatDetail;
            }
            if (didStatTab.IsSelected)
            {
                statdatagrid.ItemsSource = DIDStatDetail;
            }
            if (iidStatTab.IsSelected)
            {
                statdatagrid.ItemsSource = IIDStatDetail;
            }
            if (posStatTab.IsSelected)
            {
                statdatagrid.ItemsSource = PositionStatDetail;
            }

        }

        private void inventorySection_Collapsed(object sender, RoutedEventArgs e)
        {
            ClearDataGrids();
            packItems = new List<ItemDetail>();
            packSelectionItems.ItemsSource = packItems;
        }

        private void mainwindowSaveChar_Click(object sender, RoutedEventArgs e)
        {
            MemoryStream writeMemStream = new MemoryStream();
            BinaryWriter writeBinStream = new BinaryWriter(writeMemStream);

            weenie.Pack(writeBinStream);

            byte[] toSave = writeMemStream.ToArray();
            string saveFile = "d:\\acstuff\\" + charName.Content.ToString().Replace("Name: ", "") + "_OnSave.txt";
            File.WriteAllBytes(saveFile, toSave);

            bool saved = true; // DBUtils.SaveCharacterData(weenie.WeenieID, toSave, Application.Current.Properties["dbconnectionstring"].ToString());
            if (!saved)
            {
                MessageBox.Show($"Error saving {charName.Content.ToString().Replace("Name: ", "")}");
            }
            else
            {
                foreach (Weenie ew in weenie.Equipment.Values)
                {
                    SaveInventoryItem(ew);
                }

                foreach (Weenie iw in weenie.Inventory.Values)
                {
                    SaveInventoryItem(iw);
                }

                foreach (Weenie pw in weenie.Packs.Values)
                {
                    SaveInventoryItem(pw);
                }
            }
        }

        private void SaveInventoryItem(Weenie inventoryItem)
        {
            MemoryStream eqMemStrm = new MemoryStream();
            BinaryWriter eqBinWrtr = new BinaryWriter(eqMemStrm);
            inventoryItem.Pack(eqBinWrtr);
            byte[] itemToSave = eqMemStrm.ToArray();

            string saveFile = "d:\\acstuff\\" + inventoryItem.WeenieID + "_OnSave.txt";
            File.WriteAllBytes(saveFile, itemToSave);

            bool invSaved = true; // DBUtils.SaveInventoryItem(weenie.WeenieID, inventoryItem.WeenieID, itemToSave, Application.Current.Properties["dbconnectionstring"].ToString());
            if (!invSaved)
            {
                MessageBox.Show($"Error saving {inventoryItem.Qualities.StringStats[PropertyString.NAME_STRING]}");
            }
        }

        private void packSelectionItems_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            var item = (ItemDetail)packSelectionItems.SelectedItem;

            if (item != null)
            {
                if (item.Name != null && item.Name.Length > 0)
                {

                    if (PackInstance > 0)
                    {
                        selectedWeenie = (from d in weenie.Packs.ElementAt(PackInstance - 1).Value.Inventory
                                          where d.Value.Qualities.StringStats[PropertyString.NAME_STRING] == item.Name && d.Value.WeenieID.ToString() == item.GUID
                                          select d).FirstOrDefault().Value;
                    }
                    else if (PackInstance == 0)
                    {
                        selectedWeenie = (from d in weenie.Inventory
                                          where d.Value.Qualities.StringStats[PropertyString.NAME_STRING] == item.Name && d.Value.WeenieID.ToString() == item.GUID
                                          select d).FirstOrDefault().Value;
                    }
                    else
                    {
                        selectedWeenie = (from d in weenie.Equipment
                                          where d.Value.Qualities.StringStats[PropertyString.NAME_STRING] == item.Name && d.Value.WeenieID.ToString() == item.GUID
                                          select d).FirstOrDefault().Value;
                    }

                    if (selectedWeenie != null)
                    {
                        ClearDataGrids(true, false);
                        ResetLists();

                        packSelectedName.Content = selectedWeenie.Qualities.StringStats[PropertyString.NAME_STRING];
                        packSelectedWID.Content = selectedWeenie.WeenieID.ToString();

                        foreach (KeyValuePair<PropertyInt, uint> intst in selectedWeenie.Qualities.IntStats)
                            IntStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyInt64, ulong> intst in selectedWeenie.Qualities.Int64Stats)
                            Int64StatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyBool, bool> intst in selectedWeenie.Qualities.BoolStats)
                            BoolStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyFloat, double> intst in selectedWeenie.Qualities.FloatStats)
                            FloatStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyString, string> intst in selectedWeenie.Qualities.StringStats)
                            StringStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value });
                        foreach (KeyValuePair<PropertyDID, uint> intst in selectedWeenie.Qualities.DIDStats)
                            DIDStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyIID, uint> intst in selectedWeenie.Qualities.IIDStats)
                            IIDStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });
                        foreach (KeyValuePair<PropertyPosition, Position> intst in selectedWeenie.Qualities.PositionStats)
                            PositionStatDetail.Add(new StatDetail() { EnumName = intst.Key.ToString(), Usage = "something", Value = intst.Value.ToString() });

                        intStatTab.IsSelected = true;
                        statdatagrid.ItemsSource = IntStatDetail;

                    }
                    else
                        MessageBox.Show($"Unable to load {item.Name}. Please add error log to issues in GitLab for GDLE");
                }
            }
        }

    }
}
