﻿using DBAdminTool.Enums;
using DBAdminTool.Objects;
using DBAdminTool.WindowObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DBAdminTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Weenie weenie = null;


        public MainWindow()
        {
            InitializeComponent();
        }

        private void ManageConnection_Click(object sender, RoutedEventArgs e)
        {
            DBConnections connDialog = new DBConnections
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            connDialog.Show();
        }

        private void RemapIdConnection_Click(object sender, RoutedEventArgs e)
        {
            if (!Application.Current.Properties.Contains("dbconnectionvalid"))
            {
                MessageBox.Show("No valid database connected.\nUse Settings-Manage Connections.");
                return;
            }

            IDRemapper connDialog = new IDRemapper()
            {
                Owner = Application.Current.MainWindow,
                WindowStartupLocation = WindowStartupLocation.CenterOwner
            };
            connDialog.Show();
        }

        private void MainwindowExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void MainwindowLoadChar_Click(object sender, RoutedEventArgs e)
        {
            CharacterSelect characterSelect = new CharacterSelect();
            characterSelect.Show();
        }

    }
}
