﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Objects
{
    public class PageDataList : IPackable<PageDataList>
    {
        public uint MaxPageNumber { get; private set; }
        public uint MaxCharsPerPage { get; private set; }

        public List<PageData> Pages { get; private set; }

        public PageDataList()
        {
            Pages = new List<PageData>();
        }

        public PageDataList Unpack(BinaryReader reader)
        {
            MaxPageNumber = reader.ReadUInt32();
            MaxCharsPerPage = reader.ReadUInt32();
            uint numPages = reader.ReadUInt32();

            for (int np = 0; np < numPages; np++)
            {
                Pages.Add(new PageData().Unpack(reader));
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write((ushort)Pages.Count);
            writer.Write((ushort)16);

            foreach (PageData pd in Pages)
                pd.Pack(writer);
        }
    }
}
