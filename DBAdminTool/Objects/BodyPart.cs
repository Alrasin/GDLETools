﻿using DBAdminTool.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Objects
{
    public class BodyPart : IPackable<BodyPart>
    {
        public DAMAGE_TYPE DamageType { get; private set; }
        public uint DamageValue { get; private set; }
        public float DamageVariance { get; private set; }

        public ArmorValue ArmorValues { get; private set; }

        public BODY_HEIGHT BodyHeight { get; private set; }

        public BodyPartSelectionData PartSelection { get; private set; }

        public BodyPart()
        {

        }

        public BodyPart Unpack(BinaryReader reader)
        {
            uint internalIndex = reader.ReadUInt32();

            bool hasBPSD = Convert.ToBoolean(reader.ReadUInt32());

            DamageType = (DAMAGE_TYPE)reader.ReadUInt32();
            DamageValue = reader.ReadUInt32();
            DamageVariance = reader.ReadUInt32();
            ArmorValues = new ArmorValue().Unpack(reader);
            BodyHeight = (BODY_HEIGHT)reader.ReadUInt32();
            if (hasBPSD)
            {
                PartSelection = new BodyPartSelectionData().Unpack(reader);
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            int hasBSPD = PartSelection != null ? 1 : 0;
            writer.Write(hasBSPD);
            writer.Write((uint)DamageType);
            writer.Write(DamageValue);
            writer.Write(DamageVariance);
            ArmorValues.Pack(writer);
            writer.Write((uint)BodyHeight);
            if (hasBSPD > 0)
                PartSelection.Pack(writer);
        }
    }
}
