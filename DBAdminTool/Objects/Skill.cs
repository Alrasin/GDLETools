﻿using DBAdminTool.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Objects
{
    public class Skill : IPackable<Skill>
    {
        public uint LevelFromPP { get; private set; }
        public SKILL_ADVANCEMENT_CLASS SkillLevel { get; private set; }
        public uint PP { get; private set; }
        public uint InitialLevel { get; private set; }
        public uint LastCheckResistance { get; private set; }
        public double LastResistanceTime { get; private set; }

        public Skill()
        { }

        public Skill Unpack(BinaryReader reader)
        {
            uint ppTmp = reader.ReadUInt32();
            LevelFromPP = ppTmp & 0xFFFF;
            SkillLevel = (SKILL_ADVANCEMENT_CLASS)reader.ReadUInt32();
            PP = reader.ReadUInt32();
            InitialLevel = reader.ReadUInt32();
            LastCheckResistance = reader.ReadUInt32();
            LastResistanceTime = reader.ReadDouble();

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            uint ppTmp = (0x10000 | (LevelFromPP & 0xFFFF));
            writer.Write(ppTmp);
            writer.Write((uint)SkillLevel);
            writer.Write(PP);
            writer.Write(InitialLevel);
            writer.Write(LastCheckResistance);
            writer.Write(LastResistanceTime);
        }


    }
}
