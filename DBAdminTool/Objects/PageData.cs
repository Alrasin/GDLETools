﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Objects
{
    public class PageData : IPackable<PageData>
    {
        public uint AuthorID { get; private set; }
        public string AuthorName { get; private set; }
        public string AuthorAccount { get; private set; }
        public int TextIncluded { get; private set; }
        public int IgnoreAuthor { get; private set; }

        [DefaultValue("")]
        public string PageText { get; private set; }

        public PageData()
        { }

        public PageData Unpack(BinaryReader reader)
        {
            AuthorID = reader.ReadUInt32();
            ushort stringLength = reader.ReadUInt16();
            byte[] stringValue = reader.ReadBytes(stringLength);
            AuthorName = Encoding.Default.GetString(stringValue);
            int mod = (sizeof(ushort) + stringLength) % 4;
            if (mod != 0)
            {
                if (mod == 3)
                {
                    reader.ReadBytes(1);
                }
                if (mod == 2)
                {
                    reader.ReadBytes(2);
                }
                if (mod == 1)
                {
                    reader.ReadBytes(3);
                }

            }
            stringLength = reader.ReadUInt16();
            stringValue = reader.ReadBytes(stringLength);
            AuthorAccount = Encoding.Default.GetString(stringValue);
            mod = (sizeof(ushort) + stringLength) % 4;
            if (mod != 0)
            {
                if (mod == 3)
                {
                    reader.ReadBytes(1);
                }
                if (mod == 2)
                {
                    reader.ReadBytes(2);
                }
                if (mod == 1)
                {
                    reader.ReadBytes(3);
                }

            }

            int pageVersion = reader.ReadInt32();
            if ((pageVersion & 0xFFFF) != 0)
            {
                if ((pageVersion & 0xFFFF) == 2)
                {
                    TextIncluded = reader.ReadInt32();
                    IgnoreAuthor = reader.ReadInt32();
                }
            }
            else
            {
                TextIncluded = pageVersion;
                IgnoreAuthor = 0;
            }

            if (TextIncluded != 0)
            {
                stringLength = reader.ReadUInt16();
                stringValue = reader.ReadBytes(stringLength);
                PageText = Encoding.Default.GetString(stringValue);
                mod = (sizeof(ushort) + stringLength) % 4;
                if (mod != 0)
                {
                    if (mod == 3)
                    {
                        reader.ReadBytes(1);
                    }
                    if (mod == 2)
                    {
                        reader.ReadBytes(2);
                    }
                    if (mod == 1)
                    {
                        reader.ReadBytes(3);
                    }

                }
            }

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(AuthorID);

            ushort length = (ushort)AuthorName.Length;
            byte[] strAsBytes = AuthorName.ToByteArray();
            int alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
            writer.Write(length);
            writer.Write(strAsBytes);
            for (int ab = 0; ab < alignBytes; ab++)
            {
                writer.Write((byte)0);
            }

            length = (ushort)AuthorAccount.Length;
            strAsBytes = AuthorAccount.ToByteArray();
            alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
            writer.Write(length);
            writer.Write(strAsBytes);
            for (int ab = 0; ab < alignBytes; ab++)
            {
                writer.Write((byte)0);
            }

            writer.Write(0xFFFF0002);
            writer.Write(TextIncluded);
            writer.Write(IgnoreAuthor);

            length = (ushort)PageText.Length;
            strAsBytes = PageText.ToByteArray();
            alignBytes = Globals.ByteAlign(sizeof(ushort) + strAsBytes.Length);
            writer.Write(length);
            writer.Write(strAsBytes);
            for (int ab = 0; ab < alignBytes; ab++)
            {
                writer.Write((byte)0);
            }

        }
    }
}
