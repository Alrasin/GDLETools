﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Objects
{
    public class SubPalette : IPackable<SubPalette>
    {
        private uint defaulPaletteMask = 0x04000000;

        public uint SubPaletteID { get; private set; }
        public uint Offset { get; private set; }
        public uint NumColors { get; private set; }

        public SubPalette()
        { }

        public SubPalette Unpack(BinaryReader reader)
        {
            SubPaletteID = reader.UnpackFromUnknown(defaulPaletteMask);

            Offset = (uint)(reader.ReadByte() * 8);
            NumColors = (uint)reader.ReadByte();
            if (NumColors == 0)
                NumColors = 256;
            NumColors *= 8;

            return this;
        }

        public void Pack(BinaryWriter writer)
        {
            writer.PackToUnknown(defaulPaletteMask, SubPaletteID);
            writer.Write((byte)(Offset >> 3));
            writer.Write((byte)(NumColors >> 3));
        }
    }
}
