﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

namespace DBAdminTool
{
    public static class DBUtils
    {
        private static MySqlConnection _conn = null;
        public static void ConnectAndOpen(string connectionString)
        {
            _conn = new MySqlConnection(connectionString);
            _conn.Open();
        }

        public static void CloseDB()
        {
            if (_conn.State == ConnectionState.Open)
                _conn.Close();
        }

        public static void DeleteCharacterInventory(uint weenieId, string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.DeleteCharacterInventory, weenieId), conn))
                {
                    command.ExecuteReader();
                }
            }
        }

        public static void DeleteCharacterWeenie(uint weenieId, string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.DeleteCharacterWeenei, weenieId), conn))
                {
                    command.ExecuteReader();
                }
            }
        }

        public static void DeleteCharacter(uint weenieId, string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.DeleteCharacter, weenieId), conn))
                {
                    command.ExecuteReader();
                }
            }
        }



        public static void UpdateAccountPassAndSalt(uint accountId, string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.UpdateAccountPassword, accountId), conn))
                {
                    command.ExecuteReader();
                }
            }
        }

        public static uint GetAccountIdForCharacter(uint characterId, string connectionString)
        {
            uint retVal = 0;

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetAccountForCharacter, characterId), conn))
                {
                    DataTable dt = new DataTable();
                    dt.Load(command.ExecuteReader());
                    if (dt.Rows.Count > 0)
                        retVal = UInt32.Parse(dt.Rows[0][0].ToString().Trim());
                }
            }
            return retVal;
        }


        public static void MoveWeeniesFinal(string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.MoveWeenieNew, conn))
                {
                    command.ExecuteReader();
                }
            }
        }

        public static void SaveRemapData(uint newid, uint oldid, string connectionString)
        {
            if (_conn.State == ConnectionState.Open)
            {
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.SaveWeenieTemp, newid, oldid), _conn))
                {
                    command.ExecuteNonQuery();
                }
            }
            else
            {
                using (MySqlConnection conn = new MySqlConnection(connectionString))
                {
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.SaveWeenieTemp, newid, oldid), conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static uint GetNewId(uint oldId, string connectionString)
        {
            uint retVal = 0;

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetMappingValue, oldId), conn))
                {
                    DataTable dt = new DataTable();
                    dt.Load(command.ExecuteReader());
                    if (dt.Rows.Count > 0)
                        retVal = UInt32.Parse(dt.Rows[0][0].ToString().Trim());
                }
            }
            return retVal;
        }


        public static void AddNewWeenieTemp(uint weenieID, uint topLevelObjectId, uint blockId, byte[] dataToSave, string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.SaveWeenieTemp, weenieID, topLevelObjectId, blockId), conn))
                {
                    command.Parameters.Add("@data", MySqlDbType.Blob).Value = dataToSave;
                    int rows = command.ExecuteNonQuery();
                }
            }
        }

        public static void AddNewIdMapping(uint oldid, uint newid, string connectionString)
        {
            if (_conn.State == ConnectionState.Open)
            {
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.InsertNewMapping, newid, oldid), _conn))
                {
                    command.ExecuteNonQuery();
                }
            }
            else
            {

                using (MySqlConnection conn = new MySqlConnection(connectionString))
                {
                    //string.Format(Globals.GetCharDataLength, characterName)
                    conn.Open();
                    using (MySqlCommand command = new MySqlCommand(string.Format(Globals.InsertNewMapping, newid, oldid), conn))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
        }

        public static void DeleteOldWeenies(string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.DeleteForDefrag, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void InsertRemappedWeenies(string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.MoveWeenieNew, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public static List<uint> GetRangeEightRowId(string connectionString)
        {
            List<uint> retVal = new List<uint>();
            DataTable data = new DataTable();
            data.Columns.Add("Id", typeof(UInt32));

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                //string.Format(Globals.GetCharDataLength, characterName)
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.GetGearIds, conn))
                {
                    data.Load(command.ExecuteReader());
                }
            }
            foreach (DataRow dr in data.Rows)
            {
                retVal.Add(UInt32.Parse(dr[0].ToString().Trim()));
            }
            return retVal;
        }

        public static void DropMappingTables(string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.DropRemappingTable, conn))
                {
                    command.ExecuteNonQuery();
                }

                using (MySqlCommand command = new MySqlCommand(Globals.DropWeenieTempTable, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void CreateMappingTables(string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.CreateRemappingTable, conn))
                {
                    command.ExecuteNonQuery();
                }

                using (MySqlCommand command = new MySqlCommand(Globals.CreateWeenieTempTable, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public static bool VerifyMappingTables(string connectionString)
        {
            bool retVal = true;

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.VerifyRemappingTableExists, conn))
                {
                    DataTable dt = new DataTable();
                    dt.Load(command.ExecuteReader());
                    if (dt.Rows.Count == 0)
                        retVal = false;

                }

                using (MySqlCommand command = new MySqlCommand(Globals.VerifyTempWeenieTableExists, conn))
                {
                    DataTable dt = new DataTable();
                    dt.Load(command.ExecuteReader());
                    if (dt.Rows.Count == 0)
                        retVal = false;
                }
            }
            return retVal;
        }

        public static bool VerifyMappingFields(string connectionString)
        {
            bool retVal = true;

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.VerifyRemappingTableFields, conn))
                {
                    try
                    {
                        DataTable dt = new DataTable();
                        dt.Load(command.ExecuteReader());
                    }
                    catch
                    {
                        retVal = false;
                    }

                }

                using (MySqlCommand command = new MySqlCommand(Globals.VerifyTempWeenieTableFields, conn))
                {
                    try
                    {
                        DataTable dt = new DataTable();
                        dt.Load(command.ExecuteReader());
                    }
                    catch
                    {
                        retVal = false;
                    }
                }
            }
            return retVal;
        }

        public static List<string> GetCharacters(string connectionString)
        {
            List<string> chars = new List<string>();

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.GetCharacterList, conn))
                {
                    DataTable dt = new DataTable();
                    dt.Load(command.ExecuteReader());
                    chars = dt.GetListFromColumn("name", true);
                }
            }
            return chars;
        }

        public static Dictionary<uint, string> GetCharIdsAndNames(string connectionString)
        {
            Dictionary<uint, string> retVal = new Dictionary<uint, string>();
            DataTable dt = new DataTable();
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.GetIdAndNamesOfChars, conn))
                {
                    dt.Load(command.ExecuteReader());
                }
            }
            foreach (DataRow dr in dt.Rows)
            {
                uint rowId = UInt32.Parse(dr[0].ToString().Trim());
                string charName = dr[1].ToString().Trim();
                retVal.Add(rowId, charName);
            }
            return retVal;
        }

        public static int GetNumberOfCharacters(string connectionString)
        {
            int retVal = 0;

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.GetCharacterWeenies, conn))
                {
                    DataTable dt = new DataTable();
                    dt.Load(command.ExecuteReader());
                    retVal = dt.Rows.Count;
                }
            }
            return retVal;
        }

        public static int GetNumberOfCorpses(string connectionString)
        {
            int retVal = 0;

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.GetNumberOfCorpses, conn))
                {
                    DataTable dt = new DataTable();
                    dt.Load(command.ExecuteReader());
                    retVal = dt.Rows.Count;
                }
            }
            return retVal;
        }

        public static void DeleteCorpsesAndData(string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.DeleteCorpseRows, conn))
                {
                    command.CommandTimeout = 180;
                    command.ExecuteNonQuery();
                }
                using (MySqlCommand command = new MySqlCommand(Globals.DeleteCorpses, conn))
                {
                    command.ExecuteNonQuery();
                }
            }
        }


        public static int GetNumberOfCorpseRows(string connectionString)
        {
            int retVal = 0;

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.GetNumberOfCorpseRows, conn))
                {
                    DataTable dt = new DataTable();
                    dt.Load(command.ExecuteReader());
                    retVal = dt.Rows.Count;
                }
            }
            return retVal;
        }

        public static int GetNonCharacterRows(string connectionString)
        {
            int retVal = 0;

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(Globals.GetNonCharacterRows, conn))
                {
                    DataTable dt = new DataTable();
                    dt.Load(command.ExecuteReader());
                    retVal = Int32.Parse(dt.Rows[0][0].ToString().Trim());
                }
            }
            return retVal;
        }


        internal static DataTable GetCharacterData(object characterName, string connectionString)
        {
            DataTable charData = new DataTable();
            uint sizeOfCharArray = 0;
            byte[] rawData = new byte[1];
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();

                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetCharDataLength, characterName.ToString().Replace("'", "''")), conn))
                {
                    DataTable dataTable = new DataTable();
                    dataTable.Load(command.ExecuteReader());
                    sizeOfCharArray = Convert.ToUInt32(dataTable.Rows[0][0].ToString()) / 2;
                }

                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetCharacterData, characterName.ToString().Replace("'", "''")), conn))
                {
                    //data = command.ExecuteReader(CommandBehavior.SequentialAccess);
                    DataTable dataTable = new DataTable();
                    charData.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                    //rawData = (byte[])dataTable.Rows[0][1];
                }
            }
            return charData;
        }

        internal static DataTable GetCharInventory(object characterName, string connectionString)
        {
            DataTable inventory = new DataTable();

            inventory.Columns.Add("Id", typeof(UInt32));
            inventory.Columns.Add("Data", typeof(byte[]));

            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.GetCharInventory, characterName), conn))
                {
                    inventory.Load(command.ExecuteReader(CommandBehavior.SequentialAccess));
                }
            }

            return inventory;
        }

        internal static bool SaveCharacterData(uint weenieID, byte[] toSave, string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.SaveCharacterData, weenieID), conn))
                {
                    command.Parameters.Add("@data", MySqlDbType.Blob).Value = toSave;
                    int rows = command.ExecuteNonQuery();
                    if(rows != 1)
                        return false;
                }
            }

            return true;
        }

        internal static bool SaveInventoryItem(uint parentID, uint weenieID, byte[] itemToSave, string connectionString)
        {
            using (MySqlConnection conn = new MySqlConnection(connectionString))
            {
                conn.Open();
                using (MySqlCommand command = new MySqlCommand(string.Format(Globals.SaveInventoryData, parentID, weenieID), conn))
                {
                    command.Parameters.Add("@data", MySqlDbType.Blob).Value = itemToSave;
                    int rows = command.ExecuteNonQuery();
                    if (rows != 1)
                        return false;
                }
            }
            return true;
        }
    }
}
