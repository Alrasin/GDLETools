﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAdminTool.Enums
{
    public enum StatType
    {
        Undef_StatType,
        Int_StatType,
        Float_StatType,
        Position_StatType,
        Skill_StatType,
        String_StatType,
        DataID_StatType,
        InstanceID_StatType,
        Attribute_StatType,
        Attribute_2nd_StatType,
        BodyDamageValue_StatType,
        BodyDamageVariance_StatType,
        BodyArmorValue_StatType,
        Bool_StatType,
        Int64_StatType,
        Num_StatTypes,
        DID_StatType = DataID_StatType,
        IID_StatType = InstanceID_StatType
    };
}
