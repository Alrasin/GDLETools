﻿Welcome to the GDLE DB Admin tool.

This tool will become a method to manage characters outside of the game.

Technology:
WPF, C#
.NET verion 4.6.1

Starting: 
Inside VS - Set the solution to run mutliple projects at once. https://msdn.microsoft.com/en-us/library/ms165413.aspx
Standalone - after building, browse to the bin/[Debug|Release] folder and execute from there

Usage:
FIRST - Use the Settings menu to configure your connection to your GDLE database.  Use the "Test Connection" to verify connectivity.  Close when done.

File - Load Character: Brings up a list of characters found in the db.  Select one and click "Load Character".  The character selection will close and the main window will update.

Viewing Properties
Character - Once loaded, click the gray button to the right of the character name.  This will populate the display on the right side with all the tabs with the character property stats.

Inventory - Once loaded, expand the "Inventory" section on the left.  Click on one of the enabled buttons to display a list of items either worn, in main pack, or in sub packs.  Clicking on the
name of the item that appears in the list will populate the Property data on the right.

Not Yet Implemented:
Delete/Edit/Add Property Stats
Editing of Attributes
Editing of Skills
Other data including: Body, Spellbook, Enchantments, Events, Emotes, Creation Profiles, Page Data, Generator Data
